MUSIC - multi-scale cosmological initial conditions
===================================================

MUSIC is a computer program to generate nested grid initial conditions for
high-resolution "zoom" cosmological simulations. A detailed description
of the algorithms can be found in [Hahn & Abel (2011)][1]. You can
download the user's guide [here][3], or [read the Wiki](https://bitbucket.org/ohahn/music/wiki/Home) instead. Please consider joining the
[user mailing list][2].

Current MUSIC key features are:

- Supports output for RAMSES, ENZO, Arepo, Gadget-2/3, ART, Pkdgrav/Gasoline 
and NyX via plugins. New codes can be added.

- Support for first (1LPT) and second order (2LPT) Lagrangian perturbation 
theory, local Lagrangian approximation (LLA) for baryons with grid codes.

- Pluggable transfer functions, currently CAMB, Eisenstein&Hu, BBKS, Warm 
Dark Matter variants. Distinct baryon+CDM fields.

- Minimum bounding ellipsoid and convex hull shaped high-res regions supported 
with most codes, supports refinement mask generation for RAMSES.

- Parallelized with OpenMP
    
- Requires FFTW (v2 or v3), GSL (and HDF5 for output for some codes)

## Building MUSIC
While we still supply the old Makefile, using CMake is now the preferred way of building. 
CMake use out-of-source build, i.e. you create a build directory, and then configure the code using CMake. Inside the `music` directory, do
```
  mkdir build
  cd build
  ccmake ..
  make -j
```
to configure the code (you will se a menu), and then start a parallel compilation. If CMake has trouble finding your FFTW or HDF5 installation,
you can add hints as follows
```
  FFTW3_ROOT=<path> HDF5_ROOT=<path> ccmake ..
```
If you want to build on macOS, then it is strongly recommended to use GNU (or Intel) compilers instead of Apple's Clang. Install them e.g. via homebrew and then configure cmake to use them instead of the macOS default compiler via
```
  CC=gcc-9 CXX=g++-9 ccmake ..
```
This is necessary since Apple's compilers haven't supported OpenMP for years.


## Running

There is an example parameter file 'example.conf' in the main directory. Possible options are explained in it, it can be run
as a simple argument, e.g. from within the build directory:

```
  ./MUSIC ../ics_example.conf
```


## Disclaimer

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
or FITNESS FOR A PARTICULAR PURPOSE. By downloading and using MUSIC, you 
agree to the LICENSE, distributed with the source code in a text 
file of the same name.

[1]: http://arxiv.org/abs/1103.6031
[2]: https://groups.google.com/forum/#!forum/cosmo_music
[3]: https://bitbucket.org/ohahn/music/downloads/MUSIC_Users_Guide.pdf


# Known problems


1. `disk_cached` sets to `no` will cause `Segmentation fault (core dumped)` when computes baryon density. It is `True` by default.
2. Currently, can not load WN file from `disk_cached` output. But using the `grafic_wnoise` file is OK after open the writing option (commit: 6dc0a2f6e32cb2034429d8d78d50386df040da65). 


# Steps for generating zoomin ICs
1. Generate a cosmological simulation IC with ramdom seeds, but include this line in `[random]` option:
    ```
    grafic_out              = yes
    ```
    
  This will write out the whitenoise file in grafic format with file name, for example, `grafic_wnoise_0008.bin`
  
2. For zoomin ICs, there are two options: `box` and `ellipsoid` regions.
      
  **Option**: `box`:
      
  Setting `region                  = box` or don't set it, which is the default option. Using 
    ```
    ref_center            = 0.5,0.5,0.5
    ref_extent            = 0.2,0.2,0.2
    ```
  is the standard way to generate a box high-resolution region.
    
  **Option**: `ellipsoid` is more precise for identified halos, but require more efforts:
  
    - run the cosmological simulation ICs to z=0 and identify the object for re-simulation.
    - Using the particle IDs within the object (a slightly large radius may be more robust) to trace back to these particle positions in the cosmological simulation ICs.
    - save these particle positions in unit of simulation box size to file name `what_you_like.txt`, each line one particle position (x,y,z)
    - instead of `region = box` and `ref_center` and `ref_extent`, you need to put:
    ```
          region                  = ellipsoid
          region_point_file       = what_you_like.txt
    ```
  
  For both options, you need to put `seed[8]                 = grafic_wnoise_0008.bin` in the `[random]` option instead of random number.
  
  You may want to put `grafic_out              = yes` for saving higher level random seeds as well.
    
3. Two useful options in `[output]` for `gadget`:
    ```
    gadget_spreadcoarse     = yes
    gadget_coarsetype       = 2
    ```
    
Last, always check the final results by comparing to the cosmological run at z=0.
